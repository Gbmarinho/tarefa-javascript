function transformaNota(notas){
    var caracteres = []
    for(j of notas){
        if(j>=90){
            caracteres.push("A")
        }else if(j>=80){
            caracteres.push("B")
        }else if(j>=70){
            caracteres.push("C")
        }else if(j>=60){
            caracteres.push("D")
        }else{
            caracteres.push("F")
        }
    }
    return caracteres;
}


const notas = [78,57,20,98,78,43,91,43,27];
var notaCaracter = []
notaCaracter = transformaNota(notas);
for(k of notaCaracter){
    console.log(k)
}