function saldo(total){
    var somaReceita = 0
    var somaDespesas = 0
    for(k of total.receitas){
        somaReceita =somaReceita+k;
    }
    for(j of total.despesas){
        somaDespesas =somaDespesas+j;
    }
    console.log("Total de receita:",somaReceita,"R$")
    console.log("Total de despesas:",somaDespesas,"R$")
    if(somaReceita-somaDespesas < 0){
        console.log("saldo negativo")
    }else{
        console.log("saldo positivo")
    }
}

var total = new Object()
total.receitas = [500,800,100,300,200,1500]
total.despesas = [52,423,653,65,27,900,212,399,431]

saldo(total)