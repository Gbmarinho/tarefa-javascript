function contaNumeroDeCat(booksByCategory){
    var soma = 0;
    var somaNumLivros = 0
    var arrayNumLivros = []
    var arrayCategory = []
    for(j of booksByCategory){
        somaNumLivros = 0;
        for(k of j.books){
            somaNumLivros = somaNumLivros + 1
        }
        arrayNumLivros.push(somaNumLivros)
        arrayCategory.push(j)
        soma = soma + 1
    }
    console.log("O total de categorias diferentes sao:",soma)
    for(var n = 0;n<soma;n++){
        console.log(arrayCategory[n].category,":",arrayNumLivros[n],"livros")
    }
}

function ContNumAutores(booksByCategory){
    var contAuthor = []
    var somaAuthor = 0
    for(j of booksByCategory){
        for(k of j.books){
            if(contAuthor.indexOf(k.author) == -1){
                somaAuthor = somaAuthor + 1
                contAuthor.push(k.author)
            }
        }
    }
    console.log("Temos um total de:",somaAuthor,"Autores")
}

function ProcuraLivrosDoAutor(nome, booksByCategory){
    var somaDeLivros = 0;
    for(j of booksByCategory){
        for(k of j.books){
            if(k.author == nome){
                somaDeLivros = somaDeLivros + 1
            }
        }
    }
    console.log("Temos no nosso repertorio um total de:",somaDeLivros,"livros, do autor:",nome)
}

const booksByCategory = [
        {
            category: "Riqueza",
            books: [
                {
                    title: "Os segredos da mente milionária",
                    author: "T. Harv Eker",
                },
                {
                    title: "O homem mais rico da Babilônia",
                    author: "George S. Clason",
                },
                {
                    title: "Pai rico, pai pobre",
                    author: "Robert T. Kiyosaki e Sharon L. Lechter",
                },
            ],
            },
            {
                category: "Inteligência Emocional",
                books: [
                {
                    title: "Você é Insubstituível",
                    author: "Augusto Cury",
                },
                {
                    title: "Ansiedade – Como enfrentar o mal do século",
                    author: "Augusto Cury",
                },
                {
                    title: "Os 7 hábitos das pessoas altamente eficazes",
                    author: "Stephen R. Covey"
                }
            ]
        }

];

contaNumeroDeCat(booksByCategory);
ContNumAutores(booksByCategory);
var nome = "Augusto Cury"
ProcuraLivrosDoAutor(nome, booksByCategory)
//console.log(booksByCategory[0].category)
//console.log(booksByCategory[0].books[0].title)
// console.log(booksByCategory[0].books[0].author)
// console.log(booksByCategory[0].books)
// console.log(booksByCategory[0])
//sobreLivros(booksByCategory);