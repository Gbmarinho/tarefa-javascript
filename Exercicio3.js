function media(array){
    var media;
    var soma = 0;
    var cont = 0;
    for(let nota of array){
        soma = soma + nota; 
        cont++;
    }
    media = soma / cont;
    return media;
}
var array = [1, 5, 6];
var med = media(array);
if(med >= 6){
    console.log("Aprovado");
}else{
    console.log("Reprovado");
}
